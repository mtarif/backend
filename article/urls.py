from django.urls import path
from .views import home, category, post, page

app_name='article'

urlpatterns = [
    path('', home, name='home'),
    path('category/<str:slug>', category, name='category'),
    path('article/<str:slug>', post, name='post'),
    path('page/<str:slug>', page, name='page'),
]