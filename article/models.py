from django.db import models
from django.utils.html import format_html
from django.utils import timezone
from category.models import Category
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
import datetime, time

# Create your models here.
class Article(models.Model):
	TYPE_CHOICES = (
		('post', 'پست'),
		('page', 'برگه')
	)
	type_article = models.CharField(max_length=4, choices=TYPE_CHOICES, default='post', verbose_name="نوع")
	title = models.CharField(max_length=200, verbose_name='عنوان')
	slug = models.SlugField(unique=True, verbose_name='آدرس')
	description = models.TextField(verbose_name='خلاصه')
	content = RichTextUploadingField(verbose_name='محتوای اصلی')
	category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='دسته بندی', help_text='درصورتی که نوع "برگه" را انتخاب کرده اید، این گزینه را رها کنید.')
	author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='نویسنده')
	image = models.ImageField(upload_to=time.strftime('%Y/%m/%d'), verbose_name='عکس مطلب', null=True, blank=True)
	image_popular = ImageSpecField(source='image',
							 processors=[ResizeToFill(100, 85)],
							 format='JPEG',
							 options={'quality': 60})
	image_featured = ImageSpecField(source='image',
							 processors=[ResizeToFill(350, 240)],
							 format='JPEG',
							 options={'quality': 60})
	image_category = ImageSpecField(source='image',
							 processors=[ResizeToFill(400, 275)],
							 format='JPEG',
							 options={'quality': 60})
	image_main = ImageSpecField(source='image',
							 processors=[ResizeToFill(840, 420)],
							 format='JPEG',
							 options={'quality': 60})
	image_event = ImageSpecField(source='image',
							 processors=[ResizeToFill(400, 650)],
							 format='JPEG',
							 options={'quality': 60})
	publish = models.DateTimeField(default=timezone.now, verbose_name='تاریخ انتشار')
	status = models.BooleanField(verbose_name='نمایش داده شود؟')
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def image_tag(self):
		return format_html("<img src='{}' width=80 height=40>".format(self.image_popular.url))

	def author_name(self):
		if self.author.first_name and self.author.last_name:
			return "{} {}".format(self.author.first_name, self.author.last_name)
		elif self.author.first_name:
			return self.author.first_name
		elif self.author.last_name:
			return self.author.last_name
		else:
			return self.author

	class Meta:
		ordering = ('-publish',)
		verbose_name = "مقاله"
		verbose_name_plural = "مقالات"

	def __str__(self):
		return self.title